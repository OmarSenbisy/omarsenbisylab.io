---
title: 2. Publishing and Editing
weight: -20
---

## Publishing the website

In order to publish the website and make it public it must have a domain, so we go to **Settings>General>Advanced>Change path** then type the username of your account followed by **.gitlab.io** as shown

[![path](/media/path.png)](/media/path.png)

Then we need to change the URL in the local PC too, so we go to **Documents>website folder** as shown and open config.yaml by notepad++ **You need to install Notepad++ if you don't have it**

[![config](/media/config.png)](/media/config.png)

Then change the URL to the same username we used earlier

[![url](/media/url.png)](/media/url.png)

So now we have the website published and available online for public

## Editing the content

So after publishing you need to edit the content by some steps you do

- All sections you need to add will be shown in the **content** folder 

[![content](/media/content.png)](/media/content.png)

- You can edit any subpage from editing the **.md** file it belongs to as shown

[![about](/media/about.png)](/media/about.png)

- After editing you just click on **Save** 

[![save](/media/save.png)](/media/save.png)

- Then we go to Sourcetree to push the edits by clicking on **stage all**

[![stage](/media/stage.png)](/media/stage.png)

- Add a comment to the edit you made in order to recognize it later and click **Commit**

[![commit](/media/commit.png)](/media/commit.png)

- Finally click on Push to publish your work online

[![push](/media/push.png)](/media/push.png)

Now you have the website published

[![site](/media/site.png)](/media/site.png)

### Now all is ready for your fantastic website
