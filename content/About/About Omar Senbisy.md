---
title: About Omar Senbisy
weight: -20
---

Hello there, this is Omar Senbisy talking to you. I am a holder of bachelor degree in mechatronics engineering from MTI University and University of Wales. I was a volunteer in many activities as Maker Faire Cairo 2019,2020,2021, and FAB15 Conference. I enjoy working on mechanical designs and can spend a lot of time just designing.

<!-- spellchecker-disable -->


<!-- spellchecker-enable -->

## Why do I want to join Fab Academy?

I wanted to join Fab Academy because I love tiknering and designing so I think that joining this program will enhance my skills. And aslo because it would help me in my career as I need to know different types of manufacturing, silicone molding, prototyping, and many other things.


